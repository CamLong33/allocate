# Allocate

This app is used to allocate tasks efficiently for a group of people or, specifically, a crew on a jobsite.
As a user, you will be able to add the amount of workers you have for the day and the tasks you want completed.
The app will then structure your day to be as efficient as possible.
