//
//  NumberPickerView.swift
//  Allocate
//
//  Created by Cameron Long on 8/1/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit

protocol NumberPickerViewDelegate: class {
    func dismissNumbers()
    func didSelect(number: Int)
}

class NumberPickerView: UIView {

    @IBInspectable @IBOutlet weak var pickerView: UIPickerView!
    
    weak var delegate: NumberPickerViewDelegate!
    
    fileprivate var numbers = [Int]()
    fileprivate var selectedNumber: Int?
    
    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        delegate.dismissNumbers()
    }
    
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        delegate.didSelect(number: selectedNumber!)
        delegate.dismissNumbers()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        pickerView.dataSource = self
        pickerView.delegate = self
        for index in 1...10 {
            numbers.append(index)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
    }
    
    // Loads a XIB file into a view and returns this view.
    private func viewFromNibForClass() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension NumberPickerView: UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numbers.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}

extension NumberPickerView: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(numbers[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedNumber = numbers[row]
    }
    
}
