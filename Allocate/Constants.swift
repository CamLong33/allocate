//
//  Constants.swift
//  Allocate
//
//  Created by Cameron Long on 7/21/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation

struct Constants {
    struct Cells {
        static let Placeholder = "Placeholder Table View Cell"
        static let Task = "Task Table View Cell"
        static let Members = "Members Table View Cell"
        static let AddMember = "Add Member Table View Cell"
    }
    
    struct Segues {
        static let DisplayMembers = "displayMembers"
        static let Save = "save"
        static let Cancel = "cancel"
        static let Done = "done"
        static let ShowAddTask = "showAddTask"
        static let DisplaySchedule = "displaySchedule"
    }
    
    struct Entities {
        static let Member = "Member"
        static let Task = "Task"
    }
}
