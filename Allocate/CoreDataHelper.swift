//
//  File.swift
//  Allocate
//
//  Created by Cameron Long on 7/24/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataHelper {
    
static let appDelegate = UIApplication.shared.delegate as! AppDelegate
static let managedContext = appDelegate.persistentContainer.viewContext

    static func save() {
        do {
            try managedContext.save()
        }
        catch let error as NSError {
            print("could not save member \(error)")
        }
    }
    
    static func retrieveMembers() -> [Member] {
        let fetch = NSFetchRequest<Member>(entityName: Constants.Entities.Member)
        do {
            let members = try managedContext.fetch(fetch)
            return members
        }
        catch let error as NSError {
            print("error retrieving members \(error)")
        }
        return []
    }
    
    static func createMember() -> Member {
        let member = NSEntityDescription.insertNewObject(forEntityName: Constants.Entities.Member, into: managedContext)
        return member as! Member
    }
    
    static func deleteMember(entityName: Member) {
        managedContext.delete(entityName)
        save()
    }
    
    static func retrieveTasks() -> [Task] {
        let fetchRequest = NSFetchRequest<Task>(entityName: Constants.Entities.Task)
        do {
            let tasks = try managedContext.fetch(fetchRequest)
            return tasks
        }
        catch let error as NSError {
            print("error retrieving tasks \(error)")
        }
        return []
    }
    
    static func deleteTask(entity: Task) {
        managedContext.delete(entity)
        save()
    }
    
    static func createTask() -> Task {
        let task = NSEntityDescription.insertNewObject(forEntityName: Constants.Entities.Task, into: managedContext)
        return task as! Task
    }
    
}

