//
//  Algorithm.swift
//  Allocate
//
//  Created by Cameron Long on 8/8/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation

struct Algorithm {
    
    static func allocateTasks() -> [TimeSlot] {
        var tasks = CoreDataHelper.retrieveTasks()
        let members = CoreDataHelper.retrieveMembers()
        var slots = [TimeSlot]()
        
        var peopleCount = 0
        for task in tasks {
            peopleCount += Int(task.numberOfPeople)
        }
        
        var peopleInUse = 0
        for index in 8...17 {
            let slot = TimeSlot(hour: "\(index):00")
            while peopleInUse < members.count, tasks.count > 0{
                let task = tasks.remove(at: tasks.count - 1)
                slot.addTask(tasks: task)
                peopleInUse += Int(task.numberOfPeople)
                peopleCount = peopleCount - Int(task.numberOfPeople)
            }
            peopleInUse = 0
            slots.append(slot)
        }
        
        return slots
    }
    
}
