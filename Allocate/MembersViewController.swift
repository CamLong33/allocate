//
//  MembersViewController.swift
//  Allocate
//
//  Created by Cameron Long on 7/25/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit

class MembersViewController: UIViewController, UITableViewDelegate {
    @IBOutlet weak var membersTableView: UITableView!
    
    var members: [Member] = CoreDataHelper.retrieveMembers()

    override func viewDidLoad() {
        super.viewDidLoad()

        membersTableView.dataSource = self

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        guard let nextVC = segue.destination as? NewMemberViewController else {
            return
        }
        
        nextVC.delegate = self
    }
    

}

extension MembersViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.Members, for: indexPath) as! MembersTableViewCell
        
        let row = indexPath.row
        
        cell.memberNameLabel.text = members[row].name
        
        return cell
    }
    
    //editing style of cells
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //delete row from data model
            CoreDataHelper.deleteMember(entityName: members[indexPath.row])
            
            members.remove(at: indexPath.row)
            
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            
        }
    }

}

extension MembersViewController: NewMemberViewControllerDelegate {
    
    func didAddNewMember(memberName: String) {
        let member = CoreDataHelper.createMember()
        member.name = memberName
        members.append(member)
        membersTableView.reloadData()
        CoreDataHelper.save()
    }
    
}
