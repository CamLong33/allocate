//
//  NewMemberViewController.swift
//  Allocate
//
//  Created by Cameron Long on 7/26/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit

protocol NewMemberViewControllerDelegate: class {
    func didAddNewMember(memberName: String)
}

class NewMemberViewController: UIViewController {
    
    @IBOutlet weak var newMemberTextField: UITextField!
    
    weak var delegate: NewMemberViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        guard let memberName = newMemberTextField.text,
              memberName.characters.count > 0 else {
              return
        }
        delegate.didAddNewMember(memberName: memberName)
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
