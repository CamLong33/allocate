//
//  TimePickerView.swift
//  Allocate
//
//  Created by Cameron Long on 7/31/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit
import Foundation

protocol TimePickerViewDelegate: class {
    func dismissTime()
    func observeTime(pickerValue: TimeInterval)
}

@IBDesignable class TimePickerView: UIView {
    
    @IBOutlet weak var timePicker: UIDatePicker!
    
    weak var delegate: TimePickerViewDelegate!
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupView()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            setupView()
        }
        
    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        delegate.dismissTime()
    }
    
    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem) {
        //converts time to string in TaskViewController
        delegate.observeTime(pickerValue: timePicker.countDownDuration)
        delegate.dismissTime()
    }
    
        private func setupView() {
            let view = viewFromNibForClass()
            view.frame = bounds
            
            // Auto-layout stuff.
            view.autoresizingMask = [
                UIViewAutoresizing.flexibleWidth,
                UIViewAutoresizing.flexibleHeight
            ]
            
            // Show the view.
            addSubview(view)
        }
        
        // Loads a XIB file into a view and returns this view.
        private func viewFromNibForClass() -> UIView {
            
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
            let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
            
            return view
        }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
