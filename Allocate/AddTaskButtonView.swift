//
//  AddTaskButtonView.swift
//  Allocate
//
//  Created by Cameron Long on 7/27/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit

protocol TaskViewControllerDelegate: class {
    func willAddTask()
}

@IBDesignable class AddTaskButtonView: UIView {

    @IBOutlet weak var addTaskButton: UIButton!
    weak var delegate: TaskViewControllerDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    @IBAction func addTaskButtonTapped(_ sender: UIButton) {
        delegate.willAddTask()
        
    }
    
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
    }
    
    // Loads a XIB file into a view and returns this view.
    private func viewFromNibForClass() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
