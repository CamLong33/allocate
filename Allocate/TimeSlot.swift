//
//  TimeSlot.swift
//  Allocate
//
//  Created by Cameron Long on 8/8/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation

class TimeSlot {
    
    var tasks: [Task]
    var hour: String
    
    init(hour: String) {
        tasks = [Task]()
        self.hour = hour
    }
    
    public func addTask(tasks other: Task) {
        tasks.append(other)
    }
    
}

extension Array where Element == TimeSlot {
    
    var activeTimeSlotCount: Int {
        
        var activeTimeSlots: Int = 0
        
        for timeSlot in self {
            if timeSlot.tasks.count > 0 {
                activeTimeSlots += 1
            }
        }
        
        return activeTimeSlots
    }
    
}

