//
//  TaskViewController.swift
//  Allocate
//
//  Created by Cameron Long on 7/26/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit

class TaskViewController: UITableViewController {

    @IBOutlet weak private var titleTextField: UITextField!
    
    @IBOutlet weak fileprivate var timeTextField: UITextField!
    
    @IBOutlet weak fileprivate var numOfPeopleTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //set up for keyboard (inputView) of timeTextField
        let frame: CGRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 260)
        let datePicker: TimePickerView = TimePickerView(frame: frame)
        datePicker.delegate = self
        timeTextField.inputView = datePicker
        
        //set up for keyboard (inputView) of numOfPeopleTextField
        let numberPicker: NumberPickerView = NumberPickerView(frame: frame)
        numberPicker.delegate = self
        numOfPeopleTextField.inputView = numberPicker
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.Done {
            guard let numOfPeople = numOfPeopleTextField.text, numOfPeople.characters.count > 0, let title = titleTextField.text, title.characters.count > 0,
                let time = timeTextField.text, time.characters.count > 0
            else { return }
            
            let newTask = CoreDataHelper.createTask()
            newTask.numberOfPeople = Int16(numOfPeople)!
            newTask.title = title
            newTask.time = time
            CoreDataHelper.save()
            
            let HVCRef: HomeViewController = segue.destination as! HomeViewController
            HVCRef.refresh()
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == Constants.Segues.Done {
            guard (titleTextField.text?.characters.count)! > 0, (numOfPeopleTextField.text?.characters.count)! > 0,
                (timeTextField.text?.characters.count)! > 0
                else { return false }
        }
        return true
    }
    

}

extension TaskViewController: TimePickerViewDelegate {
    func dismissTime() {
        timeTextField.resignFirstResponder()
    }
    
    func observeTime(pickerValue: TimeInterval) {
        let valueInMinutes: Double = pickerValue / 60
        let realMinutes = Int(valueInMinutes) % 60
        let realHours = Int(valueInMinutes) / 60
        if realMinutes != 0 {
            timeTextField.text = "\(realHours) hours, \(realMinutes) minutes"
        }
        else {
            timeTextField.text = "\(realHours) hours"
        }
    }
}

extension TaskViewController: NumberPickerViewDelegate {
    func dismissNumbers() {
        numOfPeopleTextField.resignFirstResponder()
    }
    
    func didSelect(number: Int) {
        numOfPeopleTextField.text = String(number)
    }
}
