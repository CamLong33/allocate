//
//  HomeTableViewController.swift
//  Allocate
//
//  Created by Cameron Long on 7/20/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit

protocol HomeViewControllerDelegate: class {
    func refresh()
}

class HomeViewController: UIViewController {
    
    var tasks = [Task]()
    
    @IBOutlet weak fileprivate var tasksTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tasksTableView.estimatedSectionHeaderHeight = 50
        
        tasks = CoreDataHelper.retrieveTasks()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func unwindToHomeTableViewController(_ segue: UIStoryboardSegue) {
        
        
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 90
    }
    

    
    // MARK: - Navigation

    //in order to save the members to coredata
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }*/
 

}

// MARK: - DataSource

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.Task, for: indexPath) as! TaskTableViewCell
        let row = indexPath.row
        
        let task = tasks[row]
        
        cell.taskDescriptionLabel.text = task.title
        cell.estimatedTimeLabel.text = task.time
        cell.numPeopleRequiredLabel.text = String(task.numberOfPeople)
        
        return cell
    }
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let button = AddTaskButtonView(frame: CGRect(x: 0, y: 0, width: tasksTableView.frame.width, height: 50))
        //set buttons delegate reference of type TaskViewControllerDelegate to this instance of HomeViewController
        button.delegate = self
        
        return button
        
    }
    
    //editing style of cells
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //delete row from data model
            CoreDataHelper.deleteTask(entity: tasks[indexPath.row])
            
            tasks.remove(at: indexPath.row)
            
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            
        }
    }

    
}

extension HomeViewController: TaskViewControllerDelegate {
    
    func willAddTask() {
        performSegue(withIdentifier: Constants.Segues.ShowAddTask, sender: self)
    }
    
}

extension HomeViewController: HomeViewControllerDelegate {
    
    func refresh() {
        tasks = CoreDataHelper.retrieveTasks()
        tasksTableView.reloadData()
    }
}
